1. Build Image From Dockerfile
   docker build -t simple-nginx .

2. Running image with docker compose
   docker run -rm simple-nginx -p 8080:80 simple-nginx:latest

3. inspect container in newtab terminal
   docker inspect simple-nginx

4. curl docker container
   curl hostip:port -v
